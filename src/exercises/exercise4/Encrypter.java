package exercises.exercise4;

public class Encrypter {

    private ManagerMessage managerMessage;

    public Encrypter() {
        managerMessage = new ManagerMessage();
    }

    public String encrypt(String message) {
        StringBuilder encryptedMessage = new StringBuilder();

        if (managerMessage.hasSpaces(message)) {
            return encryptWithSpaces(managerMessage.getMessages(message));
        } else {
            managerMessage.setIndexLastLetter(message.length() - 1);
            encryptedMessage.append(managerMessage.getASCIICode(message.charAt(0)));

            for (int i = 1; i < message.length(); i++)
                encryptedMessage.append(managerMessage.getLastLetter(message));

            return String.valueOf(encryptedMessage);
        }
    }

    private String encryptWithSpaces(String ...messages) {
        StringBuilder encryptedMessage = new StringBuilder();

        for (String message : messages)
            encryptedMessage.append(encrypt(message)).append(" ");

        encryptedMessage.deleteCharAt(encryptedMessage.length() - 1);
        return String.valueOf(encryptedMessage);
    }
}