package exercises.exercise4;

public class Main {

    public static void main(String[] args) {
        Encrypter encrypter = new Encrypter();

        // 12olle
        System.out.println(encrypter.encrypt("Hello"));
        // 103doo
        System.out.println(encrypter.encrypt("good"));
        // 104olle 119dlro
        System.out.println(encrypter.encrypt("hello world"));
    }

}