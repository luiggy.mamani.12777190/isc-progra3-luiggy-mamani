package exercises.exercise4;

public class ManagerMessage {

    private int indexLastLetter;

    public ManagerMessage() {
        indexLastLetter = 0;
    }

    public int getASCIICode(char character) {
        return (int) character;
    }

    public char getLastLetter(String message) {
        char letter = message.charAt(indexLastLetter);
        indexLastLetter -= 1;
        return letter;
    }

    public boolean hasSpaces(String message) {
        return message.contains(" ");
    }

    public String[] getMessages(String message) {
        return message.split(" ");
    }

    public void setIndexLastLetter(int indexLastLetter) {
        this.indexLastLetter = indexLastLetter;
    }
}