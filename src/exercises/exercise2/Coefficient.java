package exercises.exercise2;

public class Coefficient {
    private int value;

    public Coefficient(int v) {
        this.value = v;
    }

    public int getValue() {
        return value;
    }

    public boolean isPositive() {
        return this.value > 0;
    }
}