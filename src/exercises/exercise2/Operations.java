package exercises.exercise2;

public class Operations {

    public Equation sumEquation(Equation firstEquation, Equation secondEquation) {
        Equation finalEquation = new Equation();
        int result = 0;

        for (Term termFirstEquation : firstEquation.getEquation()) {
            result = termFirstEquation.getCoefficientValue();

            for (Term termSecondEquation : secondEquation.getEquation()) {
                if (termFirstEquation.getVariable().equals(termSecondEquation.getVariable()))
                    result += termSecondEquation.getCoefficientValue();
            }

            finalEquation.addTerm(new Term(new Coefficient(result), termFirstEquation.getVariable()));
        }
        return finalEquation;
    }

}