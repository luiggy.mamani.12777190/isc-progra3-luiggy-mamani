package exercises.exercise2;

public class Variable {

    private char variable;

    public Variable(char variable) {
        this.variable = variable;
    }

    public char getValue() {
        if (this.variable == '\0')
            return ' ';
        return variable;
    }

    @Override
    public boolean equals(Object object) {
        Variable toCompare = (Variable) object;
        return getValue() == toCompare.getValue();
    }
}