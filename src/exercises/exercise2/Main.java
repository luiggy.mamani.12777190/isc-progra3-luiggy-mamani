package exercises.exercise2;

public class Main {
    public static void main(String[] args) {

        Equation firstEquation = new Equation();
        firstEquation.addTerms(
                new Term(new Coefficient(2), new Variable('x')),
                new Term(new Coefficient(9), new Variable('y')),
                new Term(new Coefficient(2))
        );

        Equation secondEquation = new Equation();
        secondEquation.addTerms(
                new Term(new Coefficient(5), new Variable('y')),
                new Term(new Coefficient(16), new Variable('x')),
                new Term(new Coefficient(36))
        );

        Operations operations = new Operations();
        Equation result = operations.sumEquation(firstEquation, secondEquation);

        Equation.showEquation(firstEquation);
        Equation.showEquation(secondEquation);
        Equation.showEquation(result);
    }
}