package exercises.exercise2;

import java.util.ArrayList;
import java.util.List;

public class Equation {

    private ArrayList<Term> terms;

    public Equation() {
        this.terms = new ArrayList<>();
    }

    public void addTerm(Term term) {
        terms.add(term);
    }

    public void addTerms(Term ...termsInput) {
        terms.addAll(List.of(termsInput));
    }

    public ArrayList<Term> getEquation() {
        return terms;
    }

    public static void showEquation(Equation equation) {
        StringBuilder equationString = new StringBuilder();
        for (Term term : equation.getEquation())
            equationString
                    .append(term.getCoefficient().isPositive() ? " + " : " - ")
                    .append(term.getCoefficientValue())
                    .append(term.getVariableValue());
        System.out.println(equationString);
    }
}