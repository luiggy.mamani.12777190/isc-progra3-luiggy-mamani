package exercises.exercise2;

public class Term {

    private Coefficient coefficient;
    private Variable variable;

    public Term(Coefficient coefficient, Variable variable) {
        this.coefficient = coefficient;
        this.variable = variable;
    }

    public Term(Coefficient coefficient) {
        this.coefficient = coefficient;
        this.variable = new Variable('\0');
    }

    public Coefficient getCoefficient() {
        return coefficient;
    }

    public int getCoefficientValue() {
        return coefficient.getValue();
    }

    public Variable getVariable() {
        return variable;
    }

    public char getVariableValue() {
        return variable.getValue();
    }

}