package exercises.exercise3;

public class Main {

    public static void main(String[] args) {

        LinkedList linkedList = new LinkedList();

        linkedList.addAll(
                new Node(7),
                new Node(7),
                new Node(9),
                new Node(2),
                new Node(15),
                new Node(3),
                new Node(4),
                new Node(8),
                new Node(1),
                new Node(8),
                new Node(5),
                new Node(2),
                new Node(15),
                new Node(11),
                new Node(7),
                new Node(7),
                new Node(9)
        );

        Seeker seeker = new Seeker(linkedList);
        System.out.println(seeker.findSequentialList(new int[]{2, 15}));
        System.out.println(seeker.findSequentialList(new int[]{7, 7, 9}));
    }
}