package exercises.exercise3;

import java.util.Iterator;

public class LinkedListIterator implements Iterator<Node> {

    private Node currentNode;

    public LinkedListIterator(Node headNode) {
        this.currentNode = headNode;
    }

    @Override
    public boolean hasNext() {
        return currentNode != null;
    }

    @Override
    public Node next() {
        Node node = currentNode;
        currentNode = node.getNext();
        return node;
    }
}