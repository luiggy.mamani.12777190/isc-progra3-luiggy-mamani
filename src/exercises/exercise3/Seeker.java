package exercises.exercise3;

import java.util.ArrayList;

public class Seeker {

    private LinkedList linkedList;
    private ArrayList<Integer> positions;
    private int currentPosition;

    public Seeker(LinkedList linkedList) {
        this.linkedList = linkedList;
    }

    private void initializeVariablesToFind() {
        this.currentPosition = 0;
        this.positions = new ArrayList<>();
    }

    public ArrayList<Integer> findSequentialList(int[] listToCompare) {
        initializeVariablesToFind();

        linkedList.forEach(node -> {
            if (node.hasNext() && isTheSameList(currentPosition, listToCompare)) {
                positions.add(currentPosition);
            }
            currentPosition++;
        });

        return positions;
    }

    private boolean isTheSameList(int position, int[] listSequential) {
        for (int value : listSequential) {
            int currentValue = linkedList.get(position).getValue();
            if (currentValue != value)
                return false;
            else position ++;
        }

        return true;
    }
}