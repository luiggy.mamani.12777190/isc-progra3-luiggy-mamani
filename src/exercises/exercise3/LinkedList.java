package exercises.exercise3;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class LinkedList implements Iterable<Node>{

    private Node lastNode;
    private Node headNode;

    public LinkedList() {
        this.headNode = null;
        this.lastNode = null;
    }

    public void add(Node node) {
        if (headNode == null) headNode = node;
        else lastNode.setNext(node);
        lastNode = node;
    }

    public void addAll(Node... nodes) {
        for (Node node : nodes) {
            add(node);
        }
    }

    public Node get(int index) {
        int initialIndex = 0;
        for (Node node = headNode; node != null; node = node.getNext(), initialIndex++) {
            if (initialIndex == index)
                return node;
        }
        return null;
    }

    @Override
    public Iterator<Node> iterator() {
        return new LinkedListIterator(headNode);
    }

    @Override
    public void forEach(Consumer<? super Node> action) {
        for (Node node : this) {
            action.accept(node);
        }
    }

    @Override
    public Spliterator<Node> spliterator() {
        return Iterable.super.spliterator();
    }
}