package exercises.exercise1;

public class Number {

    private int value;

    public Number(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isPositive() {
        return value > 0;
    }
}