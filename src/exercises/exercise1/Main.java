package exercises.exercise1;

import java.util.Arrays;

public class Main {
    public static void main(String args[]) {
        ArrayManager manager = new ArrayManager();
        manager.addNumbers(
                new Number(1),
                new Number(2),
                new Number(3),
                new Number(4),
                new Number(5),
                new Number(6),
                new Number(7),
                new Number(8),
                new Number(9),
                new Number(10),
                new Number(-11),
                new Number(-12),
                new Number(-13),
                new Number(-14),
                new Number(-15)
        );
        System.out.println(Arrays.toString(manager.getArrayResult()));

    }  
}  