package exercises.exercise1;

import java.util.ArrayList;
import java.util.List;

public class ArrayManager {

    private ArrayList<Number> numbers;
    private int amountPositives;
    private int sumNegatives;

    public ArrayManager() {
        amountPositives = 0;
        sumNegatives = 0;
        numbers = new ArrayList<>();
    }

    public void addNumber(Number number) {
        numbers.add(number);
    }

    public void addNumbers(Number... numbersInput) {
        numbers.addAll(List.of(numbersInput));
    }

    private int[] startManager() {
        for (Number number : numbers) {
            if (number.isPositive()) amountPositives++;
            else sumNegatives += number.getValue();
        }
        return new int[]{amountPositives, sumNegatives};
    }

    private void restartValues() {
        amountPositives = 0;
        sumNegatives = 0;
    }

    public int[] getArrayResult() {
        int[] array = startManager();
        restartValues();
        return array;
    }
}