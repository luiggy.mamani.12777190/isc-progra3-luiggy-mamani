package exercises.graphs;

import java.util.*;

public class GraphRequests<T> {

    public HashMap<Vertex<T>, LinkedList<Edge<T>>> getGraph(Graph<T> graph) {
        return graph.getGraph();
    }

    List<Map.Entry<Vertex<T>, LinkedList<Edge<T>>>> entries;
    ArrayList<Vertex<T>> mainVertices;

    public GraphRequests() {
        this.entries = null;
        this.mainVertices = new ArrayList<>();
    }

    public void getMainVertices() {
        entries.forEach(entry -> {
            mainVertices.add(entry.getKey());
        });
    }

    public ArrayList<Vertex<T>> getMainVertices(Graph<T> graph) {
        if (entries == null) startSort(graph);
        if (mainVertices.isEmpty()) getMainVertices();

        return mainVertices;
    }

    public void startSort(Graph<T> graph) {
        entries = new ArrayList<>(graph.getGraph().entrySet());

        entries.sort((o1, o2) -> {
            T value1 = o1.getKey().getValue();
            T value2 = o2.getKey().getValue();
            return ((Comparable) value1).compareTo(value2);
        });
    }

    public List<Map.Entry<Vertex<T>, LinkedList<Edge<T>>>> sortGraph(Graph<T> graph) {
        if (entries == null)
            startSort(graph);

        return entries;
    }
}