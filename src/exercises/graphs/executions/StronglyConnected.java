package exercises.graphs.executions;

import exercises.graphs.AdjacentList;
import exercises.graphs.ConnectionDeterminer;
import exercises.graphs.DirectedGraph;
import exercises.graphs.Vertex;

public class StronglyConnected {

    public static void main(String[] args) {

        Vertex<Character> vertexA = new Vertex<>('A');
        Vertex<Character> vertexB = new Vertex<>('B');
        Vertex<Character> vertexC = new Vertex<>('C');
        Vertex<Character> vertexD = new Vertex<>('D');
        Vertex<Character> vertexE = new Vertex<>('E');

        DirectedGraph<Character> graph = new DirectedGraph<>();
        graph.createManyMainVertices(vertexA, vertexB, vertexC, vertexD, vertexE);

        graph.addEdge(vertexA, vertexD);
        graph.addEdge(vertexB, vertexA);
        graph.addManyEdges(vertexC, vertexB, vertexE);
        graph.addManyEdges(vertexD, vertexC, vertexE);
        graph.addManyEdges(vertexE, vertexC, vertexA);

        AdjacentList<Character> adjacentList = new AdjacentList<>(graph);
        System.out.println(adjacentList);

        ConnectionDeterminer<Character> determiner = new ConnectionDeterminer<>();
        System.out.println("Strongly connected : " + determiner.isStronglyConnected(graph));

    }

}