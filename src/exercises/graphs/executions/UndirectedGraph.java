package exercises.graphs.executions;

import exercises.graphs.AdjacentList;
import exercises.graphs.AdjacentMatrix;
import exercises.graphs.Vertex;

public class UndirectedGraph {

    public static void main(String[] args) {

        Vertex<Character> vertexA = new Vertex<>('A');
        Vertex<Character> vertexB = new Vertex<>('B');
        Vertex<Character> vertexC = new Vertex<>('C');
        Vertex<Character> vertexD = new Vertex<>('D');
        Vertex<Character> vertexE = new Vertex<>('E');

        exercises.graphs.UndirectedGraph<Character> undirectedGraph = new exercises.graphs.UndirectedGraph<>();
        undirectedGraph.createManyMainVertices(vertexA, vertexB, vertexC, vertexD, vertexE);

        undirectedGraph.addManyEdges(vertexA, vertexB, vertexC, vertexD);
        undirectedGraph.addManyEdges(vertexB, vertexC);
        undirectedGraph.addManyEdges(vertexC, vertexE);
        undirectedGraph.addManyEdges(vertexD, vertexE);

        System.out.println("Adjacent List");
        AdjacentList<Character> adjacentListGraph = new AdjacentList<>(undirectedGraph);
        System.out.println(adjacentListGraph);

        AdjacentMatrix<Character> adjacentMatrix = new AdjacentMatrix<>(undirectedGraph);
        System.out.println("Adjacent Matrix");
        System.out.println(adjacentMatrix);
    }

}