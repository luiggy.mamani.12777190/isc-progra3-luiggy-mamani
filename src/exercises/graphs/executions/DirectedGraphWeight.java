package exercises.graphs.executions;

import exercises.graphs.*;
import exercises.graphs.DirectedGraph;

public class DirectedGraphWeight {

    public static void main(String[] args) {

        Vertex<Character> vertexA = new Vertex<>('A');
        Vertex<Character> vertexB = new Vertex<>('B');
        Vertex<Character> vertexC = new Vertex<>('C');
        Vertex<Character> vertexD = new Vertex<>('D');
        Vertex<Character> vertexE = new Vertex<>('E');
        Vertex<Character> vertexG = new Vertex<>('G');

        DirectedGraph<Character> directedGraph = new DirectedGraph<>();
        directedGraph.createManyMainVertices(
                vertexA, vertexB, vertexC, vertexD, vertexE, vertexG
        );

        directedGraph.addEdge(vertexA, vertexD, 5.0);
        directedGraph.addEdge(vertexA, vertexG, 15.0);
        directedGraph.addEdge(vertexB, vertexD, 6.0);
        directedGraph.addEdge(vertexC, vertexG, 3.0);
        directedGraph.addEdge(vertexD, vertexE, 8.0);

        System.out.println("Adjacent List");
        WeightedAdjacentList<Character> adjacentListGraph = new WeightedAdjacentList<>(directedGraph);
        System.out.println(adjacentListGraph);

        System.out.println("Adjacent Matrix");
        WeightedAdjacentMatrix<Character> adjacentMatrix = new WeightedAdjacentMatrix<>(directedGraph);
        System.out.println(adjacentMatrix);


    }

}