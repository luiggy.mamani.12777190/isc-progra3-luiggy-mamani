package exercises.graphs.executions;

import exercises.graphs.*;
import exercises.graphs.UndirectedGraph;

public class UndirectedGraphWeight {

    public static void main(String[] args) {

        Vertex<Character> vertexA = new Vertex<>('A');
        Vertex<Character> vertexB = new Vertex<>('B');
        Vertex<Character> vertexC = new Vertex<>('C');
        Vertex<Character> vertexD = new Vertex<>('D');
        Vertex<Character> vertexE = new Vertex<>('E');

        UndirectedGraph<Character> undirectedGraph = new UndirectedGraph<>();
        undirectedGraph.createManyMainVertices(vertexA, vertexB, vertexC, vertexD, vertexE);

        undirectedGraph.addEdge(vertexA, vertexB, 3.0);
        undirectedGraph.addEdge(vertexA, vertexC, 5.0);
        undirectedGraph.addEdge(vertexA, vertexD, 8.0);
        undirectedGraph.addEdge(vertexB, vertexC, 3.0);
        undirectedGraph.addEdge(vertexC, vertexE, 8.0);
        undirectedGraph.addEdge(vertexD, vertexE, 5.0);


        System.out.println("Adjacent List");
        WeightedAdjacentList<Character> adjacentListGraph = new WeightedAdjacentList<>(undirectedGraph);
        System.out.println(adjacentListGraph);

        WeightedAdjacentMatrix<Character> adjacentMatrix = new WeightedAdjacentMatrix<>(undirectedGraph);
        System.out.println("Adjacent Matrix");
        System.out.println(adjacentMatrix);
    }

}