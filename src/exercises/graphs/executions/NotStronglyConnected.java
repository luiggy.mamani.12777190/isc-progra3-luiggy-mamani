package exercises.graphs.executions;

import exercises.graphs.AdjacentList;
import exercises.graphs.ConnectionDeterminer;
import exercises.graphs.DirectedGraph;
import exercises.graphs.Vertex;

public class NotStronglyConnected {
    public static void main(String[] args) {

        Vertex<Integer> vertex1 = new Vertex<>(1);
        Vertex<Integer> vertex2 = new Vertex<>(2);
        Vertex<Integer> vertex3 = new Vertex<>(3);
        Vertex<Integer> vertex4 = new Vertex<>(4);

        DirectedGraph<Integer> graph = new DirectedGraph<>();
        graph.createManyMainVertices(vertex1, vertex2, vertex3, vertex4);

        graph.addEdge(vertex2, vertex1);
        graph.addManyEdges(vertex3, vertex2, vertex4);

        AdjacentList<Integer> adjacentList = new AdjacentList<>(graph);
        System.out.println(adjacentList);

        ConnectionDeterminer<Integer> determiner = new ConnectionDeterminer<>();
        System.out.println("Strongly connected : " + determiner.isStronglyConnected(graph));

    }

}