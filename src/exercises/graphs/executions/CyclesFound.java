package exercises.graphs.executions;

import exercises.graphs.*;
import exercises.graphs.UndirectedGraph;

public class CyclesFound {

    public static void main(String[] args) {
        Vertex<Integer> vertex1 = new Vertex<>(1);
        Vertex<Integer> vertex2 = new Vertex<>(2);
        Vertex<Integer> vertex3 = new Vertex<>(3);
        Vertex<Integer> vertex4 = new Vertex<>(4);
        Vertex<Integer> vertex5 = new Vertex<>(5);
        Vertex<Integer> vertex6 = new Vertex<>(6);

        UndirectedGraph<Integer> graph = new UndirectedGraph<>();
        graph.createManyMainVertices(
                vertex1, vertex2, vertex3, vertex4, vertex5, vertex6
        );

        graph.addManyEdges(vertex1, vertex2, vertex3);
        graph.addEdge(vertex2, vertex4);
        graph.addManyEdges(vertex3, vertex4, vertex5);
        graph.addManyEdges(vertex4, vertex5, vertex6);

        AdjacentMatrix<Integer> adjacentMatrix = new AdjacentMatrix<>(graph);
        System.out.println(adjacentMatrix);

        CycleFinder<Integer> finder = new CycleFinder<>();
        finder.findCycles(graph);
        finder.showCycles();

    }

}