package exercises.graphs.executions;

import exercises.graphs.*;
import exercises.graphs.UndirectedGraph;

public class ComparatorTrue {

    public static void main(String[] args) {

        Vertex<Integer> vertex1 = new Vertex<>(1);
        Vertex<Integer> vertex2 = new Vertex<>(2);
        Vertex<Integer> vertex3 = new Vertex<>(3);
        Vertex<Integer> vertex4 = new Vertex<>(4);
        Vertex<Integer> vertex5 = new Vertex<>(5);
        Vertex<Integer> vertex6 = new Vertex<>(6);

        UndirectedGraph<Integer> graph1 = new UndirectedGraph<>();
        graph1.createManyMainVertices(
                vertex1, vertex2, vertex3, vertex4, vertex5, vertex6
        );

        graph1.addManyEdges(vertex1, vertex2, vertex3);
        graph1.addEdge(vertex2, vertex4);
        graph1.addManyEdges(vertex3, vertex4, vertex5);
        graph1.addManyEdges(vertex4, vertex5, vertex6);


        /**
         * re-initializing the vertices so that they
         * don't have the same address in memory but the value is the same.
         */

        vertex1 = new Vertex<>(1);
        vertex2 = new Vertex<>(2);
        vertex3 = new Vertex<>(3);
        vertex4 = new Vertex<>(4);
        vertex5 = new Vertex<>(5);
        vertex6 = new Vertex<>(6);

        UndirectedGraph<Integer> graph2 = new UndirectedGraph<>();
        graph2.createManyMainVertices(
                vertex1, vertex2, vertex3, vertex4, vertex5, vertex6
        );

        graph2.addManyEdges(vertex1, vertex2, vertex3);
        graph2.addEdge(vertex2, vertex4);
        graph2.addManyEdges(vertex3, vertex4, vertex5);
        graph2.addManyEdges(vertex4, vertex5, vertex6);

        GraphComparator<Integer> comparator = new GraphComparator<>();

        AdjacentList<Integer> adjacentList = new AdjacentList<>(graph1);
        System.out.println(adjacentList);
        adjacentList = new AdjacentList<>(graph2);
        System.out.println(adjacentList);
        System.out.println("Equals : " + comparator.areEqualsGraph(graph1, graph2));

        AdjacentMatrix<Integer> adjacentMatrix1 = new AdjacentMatrix<>(graph1);
        System.out.println(adjacentMatrix1);

        AdjacentMatrix<Integer> adjacentMatrix2 = new AdjacentMatrix<>(graph2);
        System.out.println(adjacentMatrix2);

        System.out.println("Equals : " + comparator.areEqualsGraph(adjacentMatrix1, adjacentMatrix2));

    }

}