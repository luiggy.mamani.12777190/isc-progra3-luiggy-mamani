package exercises.graphs.executions;

import exercises.graphs.AdjacentList;
import exercises.graphs.AdjacentMatrix;
import exercises.graphs.Vertex;

public class DirectedGraph {

    public static void main(String[] args) {

        Vertex<Character> vertexA = new Vertex<>('A');
        Vertex<Character> vertexB = new Vertex<>('B');
        Vertex<Character> vertexC = new Vertex<>('C');
        Vertex<Character> vertexD = new Vertex<>('D');
        Vertex<Character> vertexE = new Vertex<>('E');
        Vertex<Character> vertexF = new Vertex<>('F');
        Vertex<Character> vertexG = new Vertex<>('G');
        Vertex<Character> vertexH = new Vertex<>('H');

        exercises.graphs.DirectedGraph<Character> directedGraph = new exercises.graphs.DirectedGraph<>();
        directedGraph.createManyMainVertices(
                vertexA, vertexB, vertexC, vertexD, vertexE, vertexF, vertexG, vertexH
        );

        directedGraph.addManyEdges(vertexA, vertexD, vertexG);
        directedGraph.addEdge(vertexB, vertexD);
        directedGraph.addManyEdges(vertexC, vertexG, vertexH);
        directedGraph.addManyEdges(vertexD, vertexE, vertexF, vertexH);
        directedGraph.addEdge(vertexG, vertexF);

        System.out.println("Adjacent List");
        AdjacentList<Character> adjacentListGraph = new AdjacentList<>(directedGraph);
        System.out.println(adjacentListGraph);

        System.out.println("Adjacent Matrix");
        AdjacentMatrix<Character> adjacentMatrix = new AdjacentMatrix<>(directedGraph);
        System.out.println(adjacentMatrix);


    }

}