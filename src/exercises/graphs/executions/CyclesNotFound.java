package exercises.graphs.executions;

import exercises.graphs.AdjacentMatrix;
import exercises.graphs.CycleFinder;
import exercises.graphs.UndirectedGraph;
import exercises.graphs.Vertex;

public class CyclesNotFound {

    public static void main(String[] args) {
        Vertex<Integer> vertex1 = new Vertex<>(1);
        Vertex<Integer> vertex2 = new Vertex<>(2);
        Vertex<Integer> vertex3 = new Vertex<>(3);
        Vertex<Integer> vertex4 = new Vertex<>(4);

        exercises.graphs.UndirectedGraph<Integer> graph = new UndirectedGraph<>();
        graph.createManyMainVertices(vertex1, vertex2, vertex3, vertex4);

        graph.addEdge(vertex1, vertex2);
        graph.addEdge(vertex2, vertex3);

        AdjacentMatrix<Integer> adjacentMatrix = new AdjacentMatrix<>(graph);
        System.out.println(adjacentMatrix);

        CycleFinder<Integer> finder = new CycleFinder<>();
        finder.findCycles(graph);
        finder.showCycles();

    }

}