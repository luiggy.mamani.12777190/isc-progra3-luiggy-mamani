package exercises.graphs;

public class Edge<T> {

    private Vertex<T> source;
    private Vertex<T> destination;
    private Double weight;

    public Edge(Vertex<T> source, Vertex<T> destination) {
        this.source = source;
        this.destination = destination;
        this.weight = null;
    }

    public Edge(Vertex<T> source, Vertex<T> destination, Double weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public Vertex<T> getSource() {
        return source;
    }

    public Vertex<T> getDestination() {
        return destination;
    }

    public Double getWeight() {
        return weight;
    }

    public boolean hasWeight() {
        return weight != null;
    }

    public boolean equals(Object object) {
        Edge<T> edge = (Edge<T>)object;
        return this.source.equals(edge.getSource()) && this.destination.equals(edge.getDestination());
    }
}