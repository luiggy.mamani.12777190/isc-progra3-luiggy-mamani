package exercises.graphs;

public class WeightedAdjacentList<T> extends AdjacentList<T> {

    private StringBuilder adjacentListWeight;

    public WeightedAdjacentList(Graph<T> graph) {
        super(graph);
        this.adjacentListWeight = new StringBuilder();
    }

    private void buildAdjacentListUsingWeight() {
        request.getGraph(graph).forEach((mainVertex, edges) -> {
            adjacentListWeight.append(mainVertex.getValue()).append(" --> ");
            edges.forEach(edge -> {
                adjacentListWeight
                        .append(edge.getWeight())
                        .append(" -> ");
            });
            adjacentListWeight.append("x \n");
        });
    }

    public String toString() {
        if (adjacentListWeight.isEmpty() && graph.hasWeight())
            buildAdjacentListUsingWeight();
        return adjacentListWeight.toString();
    }
}