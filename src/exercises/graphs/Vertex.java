package exercises.graphs;

public class Vertex<T> {

    private T value;
    private boolean visited;

    public Vertex(T value) {
        this.value = value;
        this.visited = false;
    }

    public T getValue() {
        return value;
    }

    public boolean equals(Object vertex) {
        return this.value.equals(((Vertex<T>)vertex).getValue());
    }

    public boolean wasVisited() {
        return visited;
    }

    public void setVisited(boolean wasVisited) {
        this.visited = wasVisited;
    }

}