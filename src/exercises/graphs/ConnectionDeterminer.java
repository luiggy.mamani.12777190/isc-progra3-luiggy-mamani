package exercises.graphs;

public class ConnectionDeterminer<T> {

    private Vertex<T> currentVertex, finalVertex;
    private boolean isReachable;

    public ConnectionDeterminer() {
        this.currentVertex = null;
        this.finalVertex = null;
        this.isReachable = false;
    }

    public boolean isReachable(Graph<T> graph, Vertex<T> vertex) {
        vertex.setVisited(true);
        for (Edge<T> edge : graph.getGraph().get(vertex)) {
            currentVertex = edge.getDestination();
            if (currentVertex.equals(finalVertex)) {
                isReachable = true;
                return isReachable;
            } else if (!currentVertex.wasVisited())
                isReachable(graph, currentVertex);
        }
        return isReachable;
    }

    public boolean isStronglyConnected(Graph<T> graph) {
        for (Vertex<T> initialVertex : graph.getGraph().keySet()) {
            for (Vertex<T> vertex : graph.getGraph().keySet()) {
                isReachable = false;
                if (!vertex.equals(initialVertex)) {
                    finalVertex = vertex;
                    if (!isReachable(graph, initialVertex))
                        return false;
                }
                graph.restartVisits();
            }
        }
        return true;
    }

}