package exercises.graphs.exercises2.exercise1;

public class Subject {

    private String name;

    public Subject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        return this.name.equals(((Subject)object).getName());
    }

    @Override
    public String toString() {
        return name;
    }

}