package exercises.graphs.exercises2.exercise1;

import exercises.graphs.Edge;
import exercises.graphs.Graph;

import java.util.ArrayList;

public class RequirementsFinder {

    private Graph<Subject> subjects;
    private ArrayList<Subject> subjectsRequirements;


    public RequirementsFinder(Graph<Subject> subjects) {
        this.subjects = subjects;
        this.subjectsRequirements = new ArrayList<>();
    }

    public ArrayList<Subject> findRequirements(Subject subjectToTake) {
        restartRequirements();
        subjects.getGraph().values().forEach(edges -> {
            edges.forEach(edge -> {
                if (hasRelationToSubject(edge, subjectToTake))
                    subjectsRequirements.add(edge.getSource().getValue());
            });
        });

        return subjectsRequirements;
    }

    private void restartRequirements() {
        subjectsRequirements.clear();
    }

    private boolean hasRelationToSubject(Edge<Subject> edge, Subject subjectToTake) {
        return edge.getDestination().getValue().equals(subjectToTake) &&
                !subjectsRequirements.contains(edge.getSource().getValue());
    }

    public void showSubjectsRequirements(Subject subjectToTake) {
        subjectsRequirements = findRequirements(subjectToTake);
        showSubjectsRequirements();
    }

    private void showSubjectsRequirements() {
        subjectsRequirements.forEach(System.out::println);
    }

}