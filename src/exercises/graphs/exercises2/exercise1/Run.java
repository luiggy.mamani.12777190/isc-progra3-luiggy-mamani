package exercises.graphs.exercises2.exercise1;

import exercises.graphs.DirectedGraph;
import exercises.graphs.Vertex;

public class Run {

    public static void main(String[] args) {

        Subject algebra = new Subject("algebra");
        Subject geometry = new Subject("geometry");
        Subject trigonometry = new Subject("trigonometry");
        Subject calculus = new Subject("calculus");
        Subject statistics = new Subject("statistics");
        Subject machineLearning = new Subject("machineLearning");

        Vertex<Subject> vertexAlgebra = new Vertex<>(algebra);
        Vertex<Subject> vertexGeometry = new Vertex<>(geometry);
        Vertex<Subject> vertexTrigonometry = new Vertex<>(trigonometry);
        Vertex<Subject> vertexCalculus = new Vertex<>(calculus);
        Vertex<Subject> vertexStatistics = new Vertex<>(statistics);
        Vertex<Subject> vertexMachineLearning = new Vertex<>(machineLearning);

        DirectedGraph<Subject> subjects = new DirectedGraph<>();
        subjects.addManyEdges(vertexAlgebra, vertexGeometry, vertexStatistics);
        subjects.addEdge(vertexGeometry, vertexTrigonometry);
        subjects.addEdge(vertexTrigonometry, vertexCalculus);
        subjects.addEdge(vertexCalculus, vertexMachineLearning);
        subjects.addEdge(vertexStatistics, vertexMachineLearning);

        RequirementsFinder finder = new RequirementsFinder(subjects);

        System.out.println("Requirements to take machine learning");
        finder.showSubjectsRequirements(machineLearning);

        System.out.println("\nRequirements to take statistics");
        finder.showSubjectsRequirements(statistics);
    }

}