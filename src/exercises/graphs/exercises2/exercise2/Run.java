package exercises.graphs.exercises2.exercise2;

import exercises.graphs.UndirectedGraph;
import exercises.graphs.Vertex;

public class Run {

    public static void main(String[] args) {

        Vertex<Integer> route1 = new Vertex<>(1);
        Vertex<Integer> route2 = new Vertex<>(2);
        Vertex<Integer> route3 = new Vertex<>(3);
        Vertex<Integer> route4 = new Vertex<>(4);
        Vertex<Integer> route5 = new Vertex<>(5);
        Vertex<Integer> route6 = new Vertex<>(6);
        Vertex<Integer> route7 = new Vertex<>(7);
        Vertex<Integer> route8 = new Vertex<>(8);

        UndirectedGraph<Integer> graph = new UndirectedGraph<>();
        graph.addEdge(route1, route4, 0.23);
        graph.addEdge(route1, route2, 0.35);
        graph.addEdge(route1, route3, 0.23);

        graph.addEdge(route2, route4, 0.74);
        graph.addEdge(route2, route3, 0.24);
        graph.addEdge(route2, route6, 0.26);

        graph.addEdge(route4, route6, 0.24);
        graph.addEdge(route5, route3, 0.51);

        graph.addEdge(route7, route3, 0.14);
        graph.addEdge(route7, route5, 0.15);

        graph.addEdge(route8, route6, 0.32);
        graph.addEdge(route8, route7, 0.32);

        RouteFinder<Integer> finder = new RouteFinder<>(graph);
        finder.showRoutes(route1, route8);

    }

}