package exercises.graphs.exercises2.exercise2;

import exercises.graphs.Edge;
import exercises.graphs.Graph;
import exercises.graphs.Vertex;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class RouteFinder<T> {

    private Graph<T> routes;
    private Vertex<T> finalRoute, nextRoute;
    private Edge<T> route;
    private ArrayList<Queue<Edge<T>>> routesFound;
    private Queue<Edge<T>> temporalRoute;
    private double weight;
    private boolean wasFond;

    public RouteFinder(Graph<T> routes) {
        this.routes = routes;
        this.finalRoute = null;
        this.route = null;
        this.routesFound = new ArrayList<>();
        this.temporalRoute = new ArrayDeque<>();
        this.weight = 0;
    }

    public ArrayList<Queue<Edge<T>>> findRoutes(Vertex<T> route1, Vertex<T> route2) {
        finalRoute = route2;
        wasFond = false;
        findRoute(route1);

        return routesFound;
    }

    private void findRoute(Vertex<T> routeToStart) {
        if (isValidToCompleteRoute(routeToStart)) {
            addRoute();
        } else {
            routeToStart.setVisited(true);
            for (Edge<T> edge : routes.getGraph().get(routeToStart)) {
                nextRoute = edge.getDestination();
                if (!nextRoute.wasVisited() && !wasFond) {
                    temporalRoute.add(edge);
                    findRoute(nextRoute);
                }
            }
        }
    }

    private boolean containRoute(Queue<Edge<T>> routeToCompare) {
        for (Queue<Edge<T>> routeSaved : routesFound) {
            if (routeToCompare.containsAll(routeSaved))
                return true;
        }

        return false;
    }

    private boolean isValidToCompleteRoute(Vertex<T> vertexToCompare) {
        return finalRoute != null &&
                vertexToCompare.equals(finalRoute) &&
                !containRoute(temporalRoute) &&
                !wasFond;
    }

    private void addRoute() {
        routesFound.add(temporalRoute);
        wasFond = true;
    }

    private void restartTemporalRoutes() {
        this.temporalRoute.clear();
    }

    public void showRoutes(Vertex<T> route1, Vertex<T> route2) {
        routesFound = findRoutes(route1, route2);
        showRoutes();
    }

    private void showRoutes() {
        routesFound.forEach(this::showRoute);
    }

    private void showRoute(Queue<Edge<T>> routes) {
        while (!routes.isEmpty()) {
            route = routes.remove();
            System.out.print(route.getSource().getValue() + " -> ");
            if (routes.isEmpty())
                System.out.print(route.getDestination().getValue());
            weight += route.getWeight();
        }
        System.out.println("\nTotal weight: " + weight);
    }

}