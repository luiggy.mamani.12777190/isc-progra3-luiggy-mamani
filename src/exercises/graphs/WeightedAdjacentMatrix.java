package exercises.graphs;

import java.util.ArrayList;

public class WeightedAdjacentMatrix<T> extends AdjacentMatrix<T> {

    private Double adjacentMatrixWeight[][];

    public WeightedAdjacentMatrix(Graph<T> graph) {
        super(graph);
    }

    private StringBuilder addValuesUsingWeight(
            StringBuilder currentBuilder,
            ArrayList<Vertex<T>> vertices
    ) {
        int currentRow = 0;
        for (Double[] row : adjacentMatrixWeight) {
            currentBuilder.append(vertices.get(currentRow).getValue()).append(" |  ");
            for (Double column : row)
                currentBuilder
                        .append((column == null) ? "." : column)
                        .append((column != null && column > 9) ? " " : "  ");
            currentRow++;
            currentBuilder.append("\n");
        }
        return currentBuilder;
    }

    public void buildAdjacentMatrixWeight() {
        adjacentMatrixWeight = getAdjacentMatrixWeight();
    }

    public String toString() {
        if (graph.hasWeight())
            return buildMatrixView().toString();
        return null;
    }

    public StringBuilder buildMatrixView() {
        StringBuilder matrixView = new StringBuilder();
        buildAdjacentMatrixWeight();
        matrixView = addHeader(matrixView, request.getMainVertices(graph));
        matrixView = addValuesUsingWeight(matrixView, request.getMainVertices(graph));
        return matrixView;
    }
}