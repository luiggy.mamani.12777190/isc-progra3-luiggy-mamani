package exercises.graphs;

import java.util.LinkedList;
import java.util.Map;

public class GraphComparator<T> {

    public boolean areEqualsGraph(AdjacentMatrix<T> graph1, AdjacentMatrix<T> graph2) {

        if (graph1.getNumberOfVertex() != graph2.getNumberOfVertex())
            return false;

        boolean[][] matrix1 = graph1.getAdjacentMatrix();
        boolean[][] matrix2 = graph2.getAdjacentMatrix();

        for (int i = 0; i < graph1.getNumberOfVertex(); i++)
            for (int j = 0; j < graph1.getNumberOfVertex(); j++)
                if (matrix1[i][j] != matrix2[i][j])
                    return false;

        return true;
    }

    public boolean areEqualsGraph(Graph<T> graph1, Graph<T> graph2) {
        if (!haveMainRequirements(graph1, graph2))
            return false;

        for (Map.Entry<Vertex<T>, LinkedList<Edge<T>>> entry : graph1.getGraph().entrySet()) {
            if (!areEqualsList(
                    entry.getValue(),
                    graph2.getUsingValue(entry.getKey())
            )) return false;
        }

        return true;
    }

    private boolean haveMainRequirements(Graph<T> graph1, Graph<T> graph2) {
        return haveSameSize(graph1, graph2) &&
                haveSameMainVertices(graph1, graph2) &&
                 areSameTypeGraph(graph1, graph2);
    }

    private boolean haveSameMainVertices(Graph<T> graph1, Graph<T> graph2) {
        for (Vertex<T> edge : graph1.getGraph().keySet())
            if (!containsKey(graph2, edge))
                return false;

        return true;
    }

    private boolean areSameTypeGraph(Graph<T> graph1, Graph<T> graph2) {
        return graph1.getClass() == graph2.getClass();
    }

    private boolean haveSameSize(Graph<T> graph1, Graph<T> graph2) {
        return graph1.size() == graph2.size();
    }

    private boolean areEqualsList(LinkedList<Edge<T>> list1, LinkedList<Edge<T>> list2) {
        if (list1 != null && list2 != null && list1.size() != list2.size())
            return false;

        for (Edge<T> edge : list1) {
            if (!contains(list2, edge))
                return false;
        }

        return true;
    }

    private boolean containsKey(Graph<T> graph, Vertex<T> vertex) {
        for (Vertex<T> mainVertex : graph.getGraph().keySet())
            if (mainVertex.equals(vertex))
                return true;

        return false;
    }

    private boolean contains(LinkedList<Edge<T>> linkedList, Edge<T> edge) {
        for (Edge<T> edgeToCompare : linkedList) {
            if (edge.equals(edgeToCompare))
                return true;
        }

        return false;
    }
}