package exercises.graphs;

import java.util.ArrayList;

public class AdjacentMatrix<T> {

    protected Graph<T> graph;
    protected GraphRequests<T> request;
    private boolean adjacentMatrix[][];
    protected Double adjacentMatrixWeight[][];
    private int numberOfVertex, row, column;
    private boolean isBuilt;

    public AdjacentMatrix(Graph<T> graph) {
        this.graph = graph;
        this.numberOfVertex = graph.getGraph().size();
        this.adjacentMatrix = new boolean[numberOfVertex][numberOfVertex];
        this.adjacentMatrixWeight = new Double[numberOfVertex][numberOfVertex];
        this.request = new GraphRequests<>();
        this.isBuilt = false;
    }

    protected void buildMatrix(boolean useWeight) {
        row = 0;
        request.sortGraph(graph).forEach(entry -> {
            entry.getValue().forEach(relation -> {
                column = 0;
                request.getMainVertices(graph).forEach(vertex -> {
                    if (relation.getDestination().equals(vertex)) {
                        if (useWeight) adjacentMatrixWeight[row][column] = relation.getWeight();
                        else adjacentMatrix[row][column] = true;
                    }
                    column++;
                });
            });
            row++;
        });
        this.isBuilt = true;
    }

    protected StringBuilder buildMatrixView() {
        StringBuilder matrixView = new StringBuilder();
        if (!isBuilt) buildAdjacentMatrix();
        matrixView = addHeader(matrixView, request.getMainVertices(graph));
        matrixView = addValues(matrixView, request.getMainVertices(graph));

        return matrixView;
    }

    protected StringBuilder addHeader(StringBuilder currentBuilder, ArrayList<Vertex<T>> vertices) {
        currentBuilder.append("  |  ");
        for (Vertex<T> vertex : vertices)
            currentBuilder.append(vertex.getValue()).append("  ");

        currentBuilder.append("\n");
        currentBuilder.append("----" + "-".repeat(vertices.size() * 3)).append("\n");
        return currentBuilder;
    }

    protected StringBuilder addValues(
            StringBuilder currentBuilder,
            ArrayList<Vertex<T>> vertices
    ) {
        int currentRow = 0;
        for (boolean[] row : adjacentMatrix) {
            currentBuilder.append(vertices.get(currentRow).getValue()).append(" |  ");
            for (boolean column : row)
                currentBuilder.append(column ? 1 : 0).append("  ");
            currentRow++;
            currentBuilder.append("\n");
        }
        return currentBuilder;
    }

    protected void buildAdjacentMatrix() {
        buildMatrix(false);
    }

    public String toString() {
        return buildMatrixView().toString();
    }

    public boolean[][] getAdjacentMatrix() {
        if (!isBuilt)
            buildMatrixView();
        return adjacentMatrix;
    }

    public int getNumberOfVertex() {
        return numberOfVertex;
    }

    public Double[][] getAdjacentMatrixWeight() {
        buildMatrix(true);
        return adjacentMatrixWeight;
    }
}