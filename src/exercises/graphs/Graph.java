package exercises.graphs;

import java.util.HashMap;
import java.util.LinkedList;

public class Graph<T> {

    protected HashMap<Vertex<T>, LinkedList<Edge<T>>> graph;
    protected int visitedVertices;
    protected boolean hasWeight;

    public HashMap<Vertex<T>, LinkedList<Edge<T>>> getGraph() {
        if (graph == null) {
            graph = new HashMap<>();
            hasWeight = false;
            visitedVertices = 0;
        }
        return graph;
    }

    public void createMainVertex(Vertex<T> mainVertex) {
        getGraph().putIfAbsent(mainVertex, new LinkedList<>());
    }

    public void createManyMainVertices(Vertex<T>... mainVertices) {
        for (Vertex<T> head : mainVertices)
            createMainVertex(head);
    }

    protected void addRelation(Vertex<T> mainVertex, Vertex<T> newVertex) {
        if (!hasRelation(mainVertex, newVertex))
            getGraph().get(mainVertex).add(new Edge<>(mainVertex, newVertex));
    }

    protected void addRelation(Vertex<T> mainVertex, Vertex<T> newVertex, Double weight) {
        if (!hasRelation(mainVertex, newVertex))
            getGraph().get(mainVertex).add(new Edge<>(mainVertex, newVertex, weight));
    }

    protected boolean hasRelation(Vertex<T> headVertex, Vertex<T> newVertex) {
        for (Edge<T> edge : getRelations(headVertex))
            if (edge.getDestination().equals(newVertex)) return true;
        return false;
    }

    protected LinkedList<Edge<T>> getRelations(Vertex<T> vertex){
        return graph.get(vertex);
    }

    protected boolean existsVertex(Vertex<T> headVertex) {
        return getGraph().get(headVertex) != null;
    }

    protected boolean hasWeight() {
        return hasWeight;
    }

    protected void setHasWeight(boolean hasWeight) {
        this.hasWeight = hasWeight;
    }

    public int size() {
        return graph.size();
    }

    public LinkedList<Edge<T>> getUsingValue(Vertex<T> vertexToGet) {
        LinkedList<Edge<T>> list = new LinkedList<>();
        for (Vertex<T> vertex : graph.keySet())
            if (vertex.equals(vertexToGet))
                list = graph.get(vertex);

        return list;
    }

    public void restartVisits() {
        for (Vertex<T> vertex : graph.keySet()) {
            vertex.setVisited(false);
        }
    }

    public boolean allWereVisited() {
        return visitedVertices >= graph.size();
    }

    public void increaseVisits() {
        this.visitedVertices++;
    }
}