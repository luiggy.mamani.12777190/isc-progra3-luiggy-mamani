package exercises.graphs;

public class DirectedGraph<T> extends Graph<T> {

    public void addEdge(Vertex<T> mainVertex, Vertex<T> newVertex, Double weight) {
        if (!existsVertex(mainVertex))
            createMainVertex(mainVertex);

        addRelation(mainVertex, newVertex, weight);
        if (!hasWeight()) setHasWeight(true);
    }

    public void addEdge(Vertex<T> mainVertex, Vertex<T> newVertex) {
        if (!existsVertex(mainVertex))
            createMainVertex(mainVertex);

        addRelation(mainVertex, newVertex);
    }

    public void addManyEdges(Vertex<T> mainVertex, Vertex<T>... newVertex) {
        for (Vertex<T> vertex : newVertex)
            addEdge(mainVertex, vertex);
    }
}