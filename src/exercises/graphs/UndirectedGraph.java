package exercises.graphs;

public class UndirectedGraph<T> extends Graph<T>{

    public void addEdge(Vertex<T> mainVertex, Vertex<T> newVertex, Double weight) {
        if (!existsVertex(mainVertex) || !existsVertex(newVertex)) {
            createMainVertex(mainVertex);
            createMainVertex(newVertex);
        }

        addRelation(mainVertex, newVertex, weight);
        addRelation(newVertex, mainVertex, weight);
        if (!hasWeight()) setHasWeight(true);
    }

    public void addEdge(Vertex<T> mainVertex, Vertex<T> newVertex) {
        if (!existsVertex(mainVertex) || !existsVertex(newVertex)) {
            createMainVertex(mainVertex);
            createMainVertex(newVertex);
        }

        addRelation(mainVertex, newVertex);
        addRelation(newVertex, mainVertex);
    }

    public void addManyEdges(Vertex<T> mainVertex, Vertex<T>... newVertex) {
        for (Vertex<T> vertex : newVertex) {
            addEdge(mainVertex, vertex);
        }
    }

}