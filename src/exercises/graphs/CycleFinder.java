package exercises.graphs;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class CycleFinder<T> {

    private ArrayList<Queue<Vertex<T>>> cycles;
    private Queue<Vertex<T>> temporalQueue;
    private Vertex<T> headVertex, vertex;
    private boolean lastVertexWasVisited;

    public CycleFinder() {
        this.temporalQueue = new ArrayDeque<>();
        this.cycles = new ArrayList<>();
        this.headVertex = null;
        this.vertex = null;
    }

    public void dfs(Graph<T> graph, Vertex<T> vertexToStart) {
        controlVisits(graph, vertexToStart);

        for (Edge<T> edge : graph.getGraph().get(vertexToStart)) {
            vertex = edge.getDestination();

            if (!vertex.wasVisited())
                dfs(graph, vertex);
            else if (isValidToCompleteTheCycle(vertex))
                completedCycle(vertex);
        }

        lastVertexWasVisited = graph.allWereVisited();
    }

    private boolean isValidToCompleteTheCycle(Vertex<T> vertexToCompare) {
        return headVertex != null &&
                vertexToCompare.equals(headVertex) &&
                temporalQueue.size() != 2 &&
                !lastVertexWasVisited;
    }

    private void controlVisits(Graph<T> graph, Vertex<T> vertex) {
        if (!vertex.wasVisited()) {
            vertex.setVisited(true);
            graph.increaseVisits();
            temporalQueue.add(vertex);
        }
    }

    private void completedCycle(Vertex<T> headVertex) {
        temporalQueue.add(headVertex);
        cycles.add(temporalQueue);
        restartQueue();
        this.headVertex = null;
    }

    public void findCycles(Graph<T> graph) {
        for (Vertex<T> vertex : graph.getGraph().keySet()) {
            lastVertexWasVisited = false;
            headVertex = vertex;
            dfs(graph, vertex);
            graph.restartVisits();
            restartQueue();
        }
    }

    private void showQueue(Queue<Vertex<T>> queue) {
        while (!queue.isEmpty())
            System.out.print(
                    queue.remove().getValue() +
                            (queue.isEmpty() ? "\n" : " -> ")
            );
    }

    public void showCycles() {
        System.out.println("Cycles : " + cycles.size());

        for (Queue<Vertex<T>> cycle : cycles)
            showQueue(cycle);
    }

    private void restartQueue() {
        this.temporalQueue = new ArrayDeque<>();
    }

}