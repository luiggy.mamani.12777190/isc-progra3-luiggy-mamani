package exercises.graphs;

public class AdjacentList<T> {

    protected GraphRequests<T> request;
    protected Graph<T> graph;
    private StringBuilder adjacentList;

    public AdjacentList(Graph<T> graph) {
        this.graph = graph;
        this.adjacentList = new StringBuilder();
        this.request = new GraphRequests<>();
    }

    private void buildAdjacentList() {
        request.getGraph(graph).forEach((mainVertex, edges) -> {
            adjacentList.append(mainVertex.getValue()).append(" --> ");
            edges.forEach(edge -> {
                adjacentList
                        .append(edge.getDestination().getValue())
                        .append(" -> ");
            });
            adjacentList.append("x \n");
        });
    }

    public String toString() {
        if (adjacentList.isEmpty())
            buildAdjacentList();
        return adjacentList.toString();
    }

}